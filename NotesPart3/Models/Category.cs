﻿using System;
using System.ComponentModel.DataAnnotations;


namespace NotesPart3.Models
{
    public class Category
    {
        public Importance Importance { get; set; }
        public ClassType ClassType { get; set; }
    }

    public enum Importance
    {
        NotImportant,
        Common,
        Important,
        VeryImportant

    }

    public enum ClassType
    {
        Lecture,
        Tutorials,
        Labolatory,
        Project
    }
}