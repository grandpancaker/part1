﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace NotesPart3.Models
{
    public class Subject
    {
        [Key] public int SubjectId { get; set; }
        public String Name { get; set; }
        public ObservableCollection<Note> Notes { get; set; }
    }
}