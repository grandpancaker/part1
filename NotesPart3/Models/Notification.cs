﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;


namespace NotesPart3.Models
{
    public class Notification
    {
        [Key]
        public int NotificationId { get; set; }
        public DateTime Date { get; set; }
        [JsonIgnore]
        public virtual Note Note { get; set; }

    }
}