namespace dotNetProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "SetionUsrHash", c => c.String());
            DropColumn("dbo.Users", "SetionHash");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "SetionHash", c => c.String());
            DropColumn("dbo.Users", "SetionUsrHash");
        }
    }
}
