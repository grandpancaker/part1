namespace dotNetProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dialogs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dialogs",
                c => new
                    {
                        DialogId = c.Long(nullable: false, identity: true),
                        NoteId = c.Int(nullable: false),
                        Sender = c.Long(nullable: false),
                        Receiver = c.Long(nullable: false),
                        User_UserId = c.Long(),
                    })
                .PrimaryKey(t => t.DialogId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MessageId = c.Long(nullable: false, identity: true),
                        Text = c.String(),
                        Author = c.Long(nullable: false),
                        Time = c.DateTime(nullable: false),
                        Dialog_DialogId = c.Long(),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("dbo.Dialogs", t => t.Dialog_DialogId)
                .Index(t => t.Dialog_DialogId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dialogs", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Messages", "Dialog_DialogId", "dbo.Dialogs");
            DropIndex("dbo.Messages", new[] { "Dialog_DialogId" });
            DropIndex("dbo.Dialogs", new[] { "User_UserId" });
            DropTable("dbo.Messages");
            DropTable("dbo.Dialogs");
        }
    }
}
