namespace dotNetProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seed : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        AttachmentId = c.Int(nullable: false, identity: true),
                        Picture = c.Binary(),
                        Note_NoteId = c.Int(),
                    })
                .PrimaryKey(t => t.AttachmentId)
                .ForeignKey("dbo.Notes", t => t.Note_NoteId)
                .Index(t => t.Note_NoteId);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        NoteId = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        Category_Importance = c.Int(nullable: false),
                        Category_ClassType = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Subject_SubjectId = c.Int(),
                    })
                .PrimaryKey(t => t.NoteId)
                .ForeignKey("dbo.Subjects", t => t.Subject_SubjectId)
                .Index(t => t.Subject_SubjectId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        NotificationId = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Note_NoteId = c.Int(),
                    })
                .PrimaryKey(t => t.NotificationId)
                .ForeignKey("dbo.Notes", t => t.Note_NoteId)
                .Index(t => t.Note_NoteId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        SubjectId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.SubjectId);
            
            CreateTable(
                "dbo.UserNotes",
                c => new
                    {
                        User_UserId = c.Long(nullable: false),
                        Note_NoteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_UserId, t.Note_NoteId })
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .ForeignKey("dbo.Notes", t => t.Note_NoteId, cascadeDelete: true)
                .Index(t => t.User_UserId)
                .Index(t => t.Note_NoteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notes", "Subject_SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.UserNotes", "Note_NoteId", "dbo.Notes");
            DropForeignKey("dbo.UserNotes", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Notifications", "Note_NoteId", "dbo.Notes");
            DropForeignKey("dbo.Attachments", "Note_NoteId", "dbo.Notes");
            DropIndex("dbo.UserNotes", new[] { "Note_NoteId" });
            DropIndex("dbo.UserNotes", new[] { "User_UserId" });
            DropIndex("dbo.Notifications", new[] { "Note_NoteId" });
            DropIndex("dbo.Notes", new[] { "Subject_SubjectId" });
            DropIndex("dbo.Attachments", new[] { "Note_NoteId" });
            DropTable("dbo.UserNotes");
            DropTable("dbo.Subjects");
            DropTable("dbo.Users");
            DropTable("dbo.Notifications");
            DropTable("dbo.Notes");
            DropTable("dbo.Attachments");
        }
    }
}
