using System.Collections.Generic;
using System.Drawing;
using dotNetProject.Model;

namespace dotNetProject.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<dotNetProject.Model.NotesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(dotNetProject.Model.NotesContext context)
        {
//            //  This method will be called after migrating to the latest version.
//
//            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
//            //  to avoid creating duplicate seed data.
//            var user = new User("adolf", "hitler");
////            var tmpAttachment = new Attachment();
////            tmpAttachment.SavePicture(Image.FromFile(@"\resource\SS-Panzer-Division_symbol.png")); //using System.Drawing;
////
////            var attachments = new List<Attachment>();
////            attachments.Add(tmpAttachment);
////
////            tmpAttachment.SavePicture(Image.FromFile(@"resource\tumblr_static_tumblr_static_3s4gdaka0bwgwsgs044cwosoc_640.png")); //using System.Drawing;
////
////            var attachments1 = new List<Attachment>();
////            attachments1.Add(tmpAttachment);
//            var NoteList = new List<Note>
//            {
////                new Note
////                {
////                    Attachments = attachments,
////                    Category = new Category {Importance = Importance.Common,ClassType = ClassType.Labolatory},
////                    CreationDate = DateTime.Today,
////                    Text =
////                        @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras semper dolor orci, sed rutrum lectus commodo vel. Nullam tempus mattis faucibus. Nullam at dolor non enim posuere faucibus. Suspendisse potenti. Donec odio odio, molestie id mi pharetra, egestas rutrum sapien. Maecenas iaculis est vel fermentum blandit. Praesent non ultricies mauris. Integer consequat laoreet neque at aliquam. Ut luctus dignissim odio in pretium. Pellentesque auctor quam eu sapien pellentesque hendrerit. Suspendisse potenti. Maecenas sed lobortis metus, quis finibus nunc."
////                },
////
////                new Note
////                {
////                    Attachments = attachments1,
////                    Category = new Category {Importance = Importance.Important,ClassType = ClassType.Lecture},
////                    CreationDate = DateTime.Today,
////                    Text =
////                        @"Mauris sit amet ipsum a diam mattis facilisis. Donec facilisis libero sit amet risus accumsan, id elementum ipsum mollis. Sed pretium sit amet turpis quis volutpat. Integer ultrices nunc ante, eget egestas dolor interdum nec. Proin turpis turpis, congue nec pretium id, pulvinar et arcu. Maecenas vitae imperdiet odio. Donec nec eleifend augue. Quisque lobortis neque vel ante fringilla, eget sagittis mauris pellentesque. Nullam at dictum orci, quis feugiat quam. Ut facilisis mi sagittis dolor aliquam, ut sodales felis vestibulum. In turpis odio, ultricies in vestibulum et, vestibulum ac lectus. In purus neque, interdum ullamcorper massa eget, volutpat sagittis ipsum. Phasellus quis rutrum velit."
////                },
//
//                new Note
//                {
//                    Category = new Category {Importance = Importance.NotImportant,ClassType = ClassType.Project},
//                    CreationDate = DateTime.Today,
//                    Text =
//                        @"Maecenas eleifend facilisis dolor at sagittis. Maecenas eget varius risus. Vestibulum et lectus quam. Aenean vitae condimentum diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis dictum feugiat efficitur. Nam viverra purus semper felis porttitor, ut sollicitudin nisi luctus. Vestibulum vitae ultrices tortor. Nunc velit massa, congue eu justo nec, sodales pharetra mauris. Nam tincidunt nibh vel velit lobortis, ut ullamcorper augue aliquam. Donec a ligula fermentum, tempor dui vel, pharetra lacus. Sed commodo ligula nibh, ac lobortis libero auctor nec. Nulla tincidunt venenatis nunc vel mollis. Vivamus feugiat et leo id aliquam. In nec mollis lectus, in tristique diam."
//                },
//
//                new Note
//                {
//                    Category = new Category {Importance = Importance.VeryImportant,ClassType = ClassType.Tutorials},
//                    CreationDate = DateTime.Today,
//                    Text =
//                        @"Etiam maximus feugiat dui in dignissim. Sed vestibulum eros at risus gravida pretium. Suspendisse pellentesque vitae mi sed ultricies. Fusce commodo risus eu justo rutrum scelerisque. Morbi tincidunt a velit in faucibus. Integer sit amet tempor libero. Nam cursus lectus ac dolor efficitur, lobortis aliquam magna eleifend. Maecenas at imperdiet velit. Vivamus bibendum tincidunt massa, quis laoreet ligula porttitor pretium. Vestibulum in magna semper, mattis felis id, fermentum dui. Nam eu pellentesque orci, eget vestibulum lorem. Aenean ipsum mauris, fringilla laoreet nunc ac, euismod congue eros. Nam id erat egestas, ornare neque ultrices, consequat nisl. Aenean ullamcorper lorem nec tellus finibus, vitae consequat neque scelerisque."
//                }
//
//            };
//
//            var subject = new Subject { Name = "DB" };
//
//            var subject2 = new Subject { Name = ".Net" };
//
//            foreach (var tmp in NoteList)
//            {
//                user.Notes.Add(tmp);
//
//                subject.Notes.Add(tmp);
//                subject2.Notes.Add(tmp);
//            }
//
//            context.Users.Add(user);
//            context.Subjects.Add(subject);
//            context.Subjects.Add(subject2);
//            context.SaveChanges();
        }
    }
}
