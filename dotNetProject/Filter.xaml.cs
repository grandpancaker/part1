﻿using dotNetProject.Model;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using dotNetProject.services.AzurServices;
using dotNetProject.services.DataBaseServices;
using dotNetProject.services.InterfaceService;

namespace dotNetProject
{
    /// <summary>
    /// Interaction logic for Filter.xaml
    /// </summary>
    public partial class Filter : Window
    {
        private INoteService noteService = new NoteAzService();
        IHubProxy hub;
        public Filter(IHubProxy _hub)
        {
            InitializeComponent();
            hub = _hub;
            Categories.ItemsSource = Enum.GetValues(typeof(Importance)).Cast<Importance>();
            ClassTypes.ItemsSource = Enum.GetValues(typeof(ClassType)).Cast<ClassType>();
        }

        private void Filtering(object sender, MouseButtonEventArgs e)
        {
            if ((Categories.SelectedItem == null) || (ClassTypes.SelectedItem == null) || (Date.SelectedDate == null))
            {
                MessageBox.Show("all fields are to be filled");
                return;
            }

            ComboBoxItem MyItem = Categories.ItemContainerGenerator.ContainerFromItem(Categories.SelectedItem) as ComboBoxItem;
            Importance imp = (Importance)MyItem.Content;
            MyItem = ClassTypes.ItemContainerGenerator.ContainerFromItem(ClassTypes.SelectedItem) as ComboBoxItem;
            ClassType type = (ClassType)MyItem.Content;
            Category cat = new Category();
            cat.Importance = imp;
            cat.ClassType = type;

            DateTime dt = (DateTime)Date.SelectedDate;

            
            var newNotes = noteService.SelectAll().Where(n => (n.Category.Importance == cat.Importance) && (n.Category.ClassType == cat.ClassType) && (n.UpdateDate == dt.Date)).ToList();

                var result = new FilterResult(hub, newNotes);
                result.Show();
            
            this.Close();
        }
    }
}
