﻿using dotNetProject.Model;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dotNetProject
{
    /// <summary>
    /// Interaction logic for AttachmentView.xaml
    /// </summary>
    public partial class AttachmentView : Window
    {
        int aId;
        public AttachmentView(int NID, int aid)
        {
            aId = aid;
            InitializeComponent();

//            using (var db = new NotesContext())
//            {
//                //Note note = db.Notes.Where(n => n.NoteId == NID).FirstOrDefault<Note>();
//
//                var at = db.Attachments.Find(aid);
//                System.Drawing.Image image = at.GetPicture();
//                using (var ms = new MemoryStream())
//                {
//                    image.Save(ms, ImageFormat.Bmp);
//                    ms.Seek(0, SeekOrigin.Begin);
//
//                    var bitmapImage = new BitmapImage();
//                    bitmapImage.BeginInit();
//                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
//                    bitmapImage.StreamSource = ms;
//                    bitmapImage.EndInit();
//
//                    Img.Source = bitmapImage;
//                }
//            }
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {
//            using (var db = new NotesContext())
//            {
//                var at = db.Attachments.Find(aId);
//                db.Attachments.Remove(at);
//                db.SaveChanges();
//            }
            this.Close();
        }
    }
}
