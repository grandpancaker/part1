﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using dotNetProject.Model;
using dotNetProject.services.DataBaseServices;
using dotNetProject.services.InterfaceService;

namespace dotNetProject
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Window
    {
        private IUserService userService = new UserDbService();
        public Register()
        {
            InitializeComponent();
        }

        private void RegisterClick(object sender, MouseButtonEventArgs e)
        {

                try
                {
                    if (password.Text == "")
                    {
                        MessageBox.Show("All fields are to be filled", "Error", MessageBoxButton.OK);
                        return;
                    }
                    if (userService.Exists( id.Text))
                    {
                        MessageBox.Show("This User already exist", "", MessageBoxButton.OK);
                    }
                    else
                    {
                        userService.Create( id.Text, password.Text);
                    }

                }
                catch (FormatException)
                {
                    MessageBox.Show("Login can only be a number", "Error", MessageBoxButton.OK);
                    return;
                }
            Close();
        }
    }
}
