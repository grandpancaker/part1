﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using dotNetProject.Model;
using System.Collections.ObjectModel;
using Microsoft.AspNet.SignalR.Client;
using dotNetProject.services.InterfaceService;
using dotNetProject.services.AzurServices;

namespace dotNetProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static string UserName = "";
        HubConnection hubConnection;
        IHubProxy hubProxy;

        //IUserService userService = new UserDbService();

        private static INoteService noteService = new NoteAzService();
        private static IUserService userService = new UserAzService();
        private static INotificationService notificationService = new NotificationAzService();
        private static ISubjectService subjectService = new SubjectAzService();
        public MainWindow()
        {
            InitializeComponent();
            ListNotes.ItemsSource = noteService.SelectAll();
            AddGroups.ItemsSource = Enum.GetValues(typeof(Importance)).Cast<Importance>();
            AddGroups.SelectedIndex = 0;
            //
                            
            //            }
        }

        private void RegisterClick(object sender, MouseButtonEventArgs e)
        {
            Register reg = new Register();
            reg.Show();
        }

        public void RefreshNoteList()
        {
            ListNotes.ItemsSource = noteService.SelectAll();
        }

        private void LoginClick(object sender, MouseButtonEventArgs e)
        {


            try
            {
                if (String.IsNullOrWhiteSpace(LoginBox.Text) || String.IsNullOrWhiteSpace(PasswordBox.Password))
                {
                    MessageBox.Show("All fields are to be filled", "Error", MessageBoxButton.OK);
                    return;
                }
                if (userService.Exists(LoginBox.Text))
                {

                    var user = userService.LogIn(LoginBox.Text, PasswordBox.Password); //ataContext.Users.First(u=>u.Name.Equals(LoginBox.Text));

                    if (user != null)
                    {
                        Login.IsEnabled = false;
                        Account.IsEnabled = true;
                        MyNotes.IsSelected = true;
                        Browse.IsEnabled = true;
                        Messages.IsEnabled = true;
                        LoginStrip.Text = "You are logged in as " + user.Name;
                        Application.Current.Properties["User"] = user;
                        var notes = noteService.SelectAll();
                        ListNotes.ItemsSource = notes;
                        foreach (var note in notes)
                        {
                            foreach (var notification in note.Notifications)
                            {
                                if (notification != null && notification.Date == DateTime.Today)
                                {
                                    MessageBox.Show("read today " + note.Name +
                                                    " it is " + note.Category.Importance +
                                                    " " + note.Category.ClassType);
                                }
                            }

                        }
                        //connect to silgnalr hub
                        hubConnection = new HubConnection("http://chatnotes.azurewebsites.net/");
                        hubProxy = hubConnection.CreateHubProxy("ChatHub");
                        hubProxy.On<string>("newMessageReceived", x => UpdateItems());
                        hubConnection.Start().Wait();
                        var me = Application.Current.Properties["User"] as User;
                        Groups.ItemsSource = me.Groups;

                    }
                    else
                    {
                        MessageBox.Show("Login or password is incorrect");
                    }
                }
                else
                {
                    MessageBox.Show("User Does Note Exists");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void UpdateItems()
        {
            var me = Application.Current.Properties["User"] as User;
            ObservableCollection<Dialog> ld = me.Dialogs;
            Participants.ItemsSource = ld;
            var d = Participants.SelectedItem as Dialog;
            MessagesList.ItemsSource = d.Messages;
        }

        private void NewClick(object sender, MouseButtonEventArgs e)
        {
            NoteView nv = new NoteView(hubProxy);
            nv.Show();
            ListNotes.ItemsSource = noteService.SelectAll();
            ListNotes.Items.Refresh();
            
        }

        private void FilterClick(object sender, MouseButtonEventArgs e)
        {
            Filter filter = new Filter(hubProxy);
            filter.Show();
        }

        private void OpenNote(object sender, SelectionChangedEventArgs e)
        {
            if (ListNotes.SelectedItem != null)
            {
                var n = ListNotes.SelectedItem as Note;
                var noteId = n.NoteId;
                NoteView nv = new NoteView(hubProxy, noteId);
                nv.Show();
                ListNotes.SelectedItem = null;
            }
        }

        private void UpdatePassword(object sender, MouseButtonEventArgs e)
        {
            var me = Application.Current.Properties["User"] as User;
            if (userService.CheckPassword(OldPswd.Text))
            {
                if (!String.IsNullOrWhiteSpace(NewPswd.Text))
                {
                    userService.SetNewPassword(NewPswd.Text);
                    MessageBox.Show("Password changed");
                }
                else MessageBox.Show("New password can't be null");
            }
            else MessageBox.Show("Incorrect old password");

        }

        //set itemssource of GroupNotes to collection of notes from users of chosen group
        private void ChooseClick(object sender, MouseButtonEventArgs e)
        {

        }
        private void JoinGroup(object sender, MouseButtonEventArgs e)
        {
            //using (var db = new NotesContext())
            //{
            //    var me = Application.Current.Properties["User"] as User;
            //    if(me.Groups==null)
            //    {
            //        ObservableCollection<Group> gr = new ObservableCollection<Group>();
            //        me.Groups = gr;
            //    }
            //    me.Groups.Add((Group)AddGroups.SelectedIndex);
            //    db.SaveChanges();
            //}

        }

        //send chat message
        private void SendClick(object sender, MouseButtonEventArgs e)
        {
            //to db
            //var me = Application.Current.Properties["User"] as User;
            //Dialog dialog = dialogService.Select(Where...);
            //Message mess = new Message { Author = me.UserId, Text = MessageTxtBox.Text, Time = DateTime.Now, Dialog_DialogId = dialog.DialogId };
            //messageService.Create(mess);

            //to hub
            hubProxy.Invoke("BroadcastMessageToAll", "").Wait();

            MessageTxtBox.Text = "";
        }
    }
}
