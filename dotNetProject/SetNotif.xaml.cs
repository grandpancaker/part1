﻿using dotNetProject.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using dotNetProject.services.DataBaseServices;
using dotNetProject.services.InterfaceService;

namespace dotNetProject
{
    /// <summary>
    /// Interaction logic for SetNotif.xaml
    /// </summary>
    public partial class SetNotif : Window
    {
        int nid;
        private INoteService noteService = new NoteDbService();
        public SetNotif(int id)
        {
            InitializeComponent();
            nid = id;
        }

        private void Set(object sender, MouseButtonEventArgs e)
        {
            DateTime date = (DateTime)(Date.SelectedDate);

            Notification n = new Notification
            {
                Date = date.Date,
            };
            NoteView.listn.Add(n);
     
            this.Close();
        }
    }
}
