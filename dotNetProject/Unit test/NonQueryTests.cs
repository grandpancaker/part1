using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using dotNetProject.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Drawing;

namespace dotNetProject.Unit_test
{
    [TestClass]
    public class NonQueryTests
    {
        [TestMethod]
        public void CreateNote_saves_a_Note_via_context()
        {
            var mockSet = new Mock<DbSet<Note>>();

            var mockContext = new Mock<NotesContext>();
            mockContext.Setup(m => m.Notes).Returns(mockSet.Object);

            var service = new NotesService(mockContext.Object);
            service.AddNote("adolf");

            mockSet.Verify(m => m.Add(It.IsAny<Note>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void CreateNoteANoteViaContextAndAddNotification()
        {
            var mockSetNotification = new Mock<DbSet<Notification>>();

            var mockContext = new Mock<NotesContext>();
            mockContext.Setup(m => m.Notifications).Returns(mockSetNotification.Object);

            var service = new NotesService(mockContext.Object);
            service.AddNotification(DateTime.Today.AddDays(6));

            mockSetNotification.Verify(m => m.Add(It.IsAny<Notification>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void CreateUser_via_context()
        {
            var mockSet = new Mock<DbSet<User>>();

            var mockContext = new Mock<NotesContext>();
            mockContext.Setup(m => m.Users).Returns(mockSet.Object);

            var service = new NotesService(mockContext.Object);
            service.AddUser("adolf","Hitler");

            mockSet.Verify(m => m.Add(It.IsAny<User>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void CreateAtt_via_context()
        {
            var mockSet = new Mock<DbSet<Attachment>>();

            var mockContext = new Mock<NotesContext>();
            mockContext.Setup(m => m.Attachments).Returns(mockSet.Object);

            var service = new NotesService(mockContext.Object);
            var image = Image.FromFile(@"C:\Users\HP pc\Documents\Visual Studio 2015\Projects\Notes1\part1\dotNetProject\resource\SS-Panzer-Division_symbol.png");
            service.AddAttachment(image);

            mockSet.Verify(m => m.Add(It.IsAny<Attachment>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void CreateSubject_via_context()
        {
            var mockSet = new Mock<DbSet<Subject>>();

            var mockContext = new Mock<NotesContext>();
            mockContext.Setup(m => m.Subjects).Returns(mockSet.Object);

            var service = new NotesService(mockContext.Object);
            service.AddSubject("adolf");

            mockSet.Verify(m => m.Add(It.IsAny<Subject>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }


        [TestMethod]
        public void GetAllSubjects_orders_by_name()
        {
            var subject = new List<Subject>
            {
                new Subject {Name = "BBB"},
                new Subject {Name = "ZZZ"},
                new Subject {Name = "AAA"},
            }.AsQueryable();
            var mockSet = new Mock<DbSet<Subject>>();

            mockSet.As<IQueryable<Subject>>().Setup(m => m.Provider).Returns(subject.Provider);
            mockSet.As<IQueryable<Subject>>().Setup(m => m.Expression).Returns(subject.Expression);
            mockSet.As<IQueryable<Subject>>().Setup(m => m.ElementType).Returns(subject.ElementType);
            mockSet.As<IQueryable<Subject>>().Setup(m => m.GetEnumerator()).Returns(subject.GetEnumerator());
            var mockContext = new Mock<NotesContext>();
            mockContext.Setup(m => m.Subjects).Returns(mockSet.Object);

            var service = new NotesService(mockContext.Object);
            var Subject = service.GetAllSubject();

            Assert.AreEqual(3, Subject.Count);
            Assert.AreEqual("AAA", Subject[0].Name);
            Assert.AreEqual("BBB", Subject[1].Name);
            Assert.AreEqual("ZZZ", Subject[2].Name);
        }


        [TestMethod]
        public void GetAllNotes_orders_by_name()
        {
            var note = new List<Note>
            {
                new Note {Name = "BBB"},
                new Note {Name = "ZZZ"},
                new Note {Name = "AAA"},
            }.AsQueryable();
            var mockSet = new Mock<DbSet<Note>>();

            mockSet.As<IQueryable<Note>>().Setup(m => m.Provider).Returns(note.Provider);
            mockSet.As<IQueryable<Note>>().Setup(m => m.Expression).Returns(note.Expression);
            mockSet.As<IQueryable<Note>>().Setup(m => m.ElementType).Returns(note.ElementType);
            mockSet.As<IQueryable<Note>>().Setup(m => m.GetEnumerator()).Returns(note.GetEnumerator());
            var mockContext = new Mock<NotesContext>();
            mockContext.Setup(m => m.Notes).Returns(mockSet.Object);

            var service = new NotesService(mockContext.Object);
            var Note = service.GetAllNotes();

            Assert.AreEqual(3, Note.Count);
            Assert.AreEqual("AAA", Note[0].Name);
            Assert.AreEqual("BBB", Note[1].Name);
            Assert.AreEqual("ZZZ", Note[2].Name);
        }

        [TestMethod]
        public void GetAllUsers_orders_by_name()
        {
            var user = new List<User>
            {
                new User {Name = "BBB", Password = "a"},
                new User {Name = "ZZZ", Password = "a"},
                new User {Name = "AAA", Password = "a"},
            }.AsQueryable();
            var mockSet = new Mock<DbSet<User>>();

            mockSet.As<IQueryable<User>>().Setup(m => m.Provider).Returns(user.Provider);
            mockSet.As<IQueryable<User>>().Setup(m => m.Expression).Returns(user.Expression);
            mockSet.As<IQueryable<User>>().Setup(m => m.ElementType).Returns(user.ElementType);
            mockSet.As<IQueryable<User>>().Setup(m => m.GetEnumerator()).Returns(user.GetEnumerator());
            var mockContext = new Mock<NotesContext>();
            mockContext.Setup(m => m.Users).Returns(mockSet.Object);

            var service = new NotesService(mockContext.Object);
            var User = service.GetAllUsers();

            Assert.AreEqual(3,     User.Count);
            Assert.AreEqual("AAA", User[0].Name);
            Assert.AreEqual("BBB", User[1].Name);
            Assert.AreEqual("ZZZ", User[2].Name);
        }
    }
}
