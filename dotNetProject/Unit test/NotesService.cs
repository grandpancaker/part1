using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using dotNetProject.Model;

namespace dotNetProject.Unit_test
{
    public class NotesService
    {
        private readonly NotesContext _context;

        public NotesService(NotesContext context)
        {
            _context = context;
        }

        public Note AddNote(string name)
        {
            var note = _context.Notes.Add(new Note { Name = name});
            _context.SaveChanges();

            return note;
        }

        public Attachment AddAttachment(System.Drawing.Image imageIn)
        {
            var tmp = new Attachment();
            tmp.SavePicture(imageIn);
            var Attachment = _context.Attachments.Add(tmp);
            _context.SaveChanges();

            return Attachment;
        }

        public User AddUser(string name, string password)
        {
            var user = _context.Users.Add(new User { Name = name, Password = password });
            _context.SaveChanges();

            return user;
        }

        public Subject AddSubject(string name)
        {
            var subject = _context.Subjects.Add(new Subject { Name = name});
            _context.SaveChanges();

            return subject;
        }

        public Notification AddNotification(DateTime date)
        {
            var Notification = _context.Notifications.Add(new Notification { Date = date });
            _context.SaveChanges();

            return Notification;
        }
        public List<Note> GetAllNotes()
        {
            var query = from b in _context.Notes
                orderby b.Name
                select b;

            return query.ToList();
        }

        public List<User> GetAllUsers()
        {
            var query = from b in _context.Users
                        orderby b.Name
                select b;

            return query.ToList();
        }

        public List<Subject> GetAllSubject()
        {
            var query = from b in _context.Subjects
                        orderby b.Name
                select b;

            return query.ToList();
        }

        public async Task<List<Note>> GetAllNotesAsync()
        {
            var query = from b in _context.Notes
                orderby b.Name
                select b;

            return await query.ToListAsync();
        }


        public async Task<List<Note>> GetAllUsersAsync()
        {
            var query = from b in _context.Notes
                orderby b.Name
                select b;

            return await query.ToListAsync();
        }
    }
}