using dotNetProject.Model;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dotNetProject
{
    /// <summary>
    /// Interaction logic for Comment.xaml
    /// </summary>
    public partial class Comment : Window
    {
        IHubProxy hubProxy;
        int nid, author;
        int sender;
        public Comment(IHubProxy hub, int _nid, int _author, int _sender)
        {
            InitializeComponent();
            hubProxy = hub;
            nid = _nid;
            author = _author;
            sender = _sender;
        }

        private void CommentClick(object sender, MouseButtonEventArgs e)
        {
            //do db
            //Dialog dialog = new Dialog { NoteId = nid, Receiver = author, Sender = (int)sender};
            //dialogService.Create(dialog);

            //Message mess = new Message { Author = (int)sender, Text = CommentTxt.Text, Time = DateTime.Now, Dialog_DialogId = dialog.DialogId };
            //messageService.Create(mess);

            //send to hub
            hubProxy.Invoke("BroadcastMessageToAll", "").Wait();
            MessageBox.Show("Your comment was sent.\nYou can see this dialog in Messages tab.", "Sent");
            this.Close();
        }
    }
}
