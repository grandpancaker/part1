﻿using dotNetProject.Model;
using Microsoft.AspNet.SignalR.Client;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace dotNetProject
{
    /// <summary>
    /// Interaction logic for FilterResult.xaml
    /// </summary>
    public partial class FilterResult : Window
    {
        IHubProxy hub;
        public FilterResult(IHubProxy _hub, List<Note> list)
        {
            InitializeComponent();
            hub = _hub;
            ListNotes.ItemsSource = list;
        }

        private void OpenNote(object sender, SelectionChangedEventArgs e)
        {
            ListBoxItem MyItem = ListNotes.ItemContainerGenerator.ContainerFromItem(ListNotes.SelectedItem) as ListBoxItem;
            if (MyItem == null) return;
            Note n = MyItem.Content as Note;

            var user = Application.Current.Properties["User"] as User;
            ListNotes.DataContext = user.Notes;


            NoteView nv = new NoteView(hub, n.NoteId);
            nv.Show();
        }
    }
}
