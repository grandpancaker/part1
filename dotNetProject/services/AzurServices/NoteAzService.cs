using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using dotNetProject.Model;
using dotNetProject.services.InterfaceService;
using Newtonsoft.Json;

namespace dotNetProject.services.AzurServices
{
    public class NoteAzService : INoteService
    {
        public bool Create(Note note)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Note/Create";

            var content = JsonConvert.SerializeObject(note);
            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(URL, httpContent);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool Update(Note note)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Note/Update";

            var content = JsonConvert.SerializeObject(note);
            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(URL, httpContent);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public List<Note> SelectAll()
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Note/SelectAll";
            var test = "";

            using (var client = new HttpClient())
            {
                Task<List<Note>> task = Task.Run(async () =>
                {
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    test = responseText;
                    return JsonConvert.DeserializeObject<List<Note>>(responseText);
                });
                task.Wait();


                return task.Result;
            }
        }

        public List<Note> SelectAllForUser(int userId)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Note/SelectAllForUser/"+userId;


            using (var client = new HttpClient())
            {
                Task<List<Note>> task = Task.Run(async () =>
                {
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<Note>>(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public Note Select(int NId)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Note/Select/" + NId;


            using (var client = new HttpClient())
            {
                Task<Note> task = Task.Run(async () =>
                {
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<Note>(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool Delete(int id)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Note/delete/" + id;

            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var response = await client.DeleteAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }
    }
}