﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using dotNetProject.Model;
using dotNetProject.services.InterfaceService;
using Newtonsoft.Json;

namespace dotNetProject.services.AzurServices
{
    class NotificationAzService : INotificationService
    {
      
        public bool Create(Notification notification)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/notification/Create";

            var content = JsonConvert.SerializeObject(notification);
            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(URL, httpContent);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool Update(Notification notification)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/notification/Update";

            var content = JsonConvert.SerializeObject(notification);
            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(URL, httpContent);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public List<Notification> Select()
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Notification/SelectAll";


            using (var client = new HttpClient())
            {
                Task<List<Notification>> task = Task.Run(async () =>
                {
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<Notification>>(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public List<Notification> Select(int NId)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Notification/Select/" + NId;


            using (var client = new HttpClient())
            {
                Task<List<Notification>> task = Task.Run(async () =>
                {
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<Notification>>(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool Delete(int id)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Notification/delete/" + id;

            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var response = await client.DeleteAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool DeleteByNote(int id)
        {
            throw new NotImplementedException();
        }
    }
}
