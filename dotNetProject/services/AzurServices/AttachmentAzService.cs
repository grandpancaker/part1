﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using dotNetProject.Model;
using dotNetProject.services.InterfaceService;
using Newtonsoft.Json;

namespace dotNetProject.services.AzurServices
{
    class AttachmentAzService :IAttachmentService
    {
        public Attachment GetAttachment(int id)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Attachment/GetAttachment/"+id;

            using (var client = new HttpClient())
            {
                Task<Attachment> task = Task.Run(async () =>
                {
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<Attachment>(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public List<Attachment> GetAttachmentsForNote(int noteId)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Attachment/GetAttachmentsForNote/"+noteId;

            
            using (var client = new HttpClient())
            {
                Task<List<Attachment>> task = Task.Run(async () =>
                {
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<Attachment>>(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool AddAttach(Attachment at, int noteId)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Attachment/AddAttach/"+noteId;

            var content = JsonConvert.SerializeObject(at);
            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(URL, httpContent);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool Create(Attachment at)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Attachment/Create";

            var content = JsonConvert.SerializeObject(at);
            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(URL, httpContent);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool Delete(int id)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/Attachment/Delete/"+id;

            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var response = await client.DeleteAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool DeleteByNote(int id)
        {
            throw new NotImplementedException();
        }
    }
}
