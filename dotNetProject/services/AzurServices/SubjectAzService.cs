using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using dotNetProject.Model;
using dotNetProject.services.InterfaceService;
using Newtonsoft.Json;

namespace dotNetProject.services.AzurServices
{
    public class SubjectAzService : ISubjectService
    {
        public bool Create(Subject subject)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/subject/Create";

            var content = JsonConvert.SerializeObject(subject);
            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(URL, httpContent);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean( responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool Update(Subject subject)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/subject/Update";
            string serializedObject = new JavaScriptSerializer().Serialize(subject);
            HttpWebRequest request = WebRequest.CreateHttp(URL);
            request.Method = "PUT";
            request.AllowWriteStreamBuffering = false;
            request.ContentType = "application/json";
            request.Accept = "Accept=application/json";
            request.SendChunked = false;
            request.ContentLength = serializedObject.Length;
            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(serializedObject);
            }
            var response = request.GetResponse() as HttpWebResponse;
            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                return serializer.Deserialize<bool>(reader.ReadToEnd());
            }
        }

        public Subject Select(int id)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/subject/select/"+id;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.ContentType = "application/json; charset=utf-8";

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                return serializer.Deserialize<Subject>(reader.ReadToEnd());
            }
        }

        public List<Subject> SelectAll()
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/subject/selectall";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.ContentType = "application/json; charset=utf-8";
//            request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes("username:password"));
//            request.PreAuthenticate = true;
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                return serializer.Deserialize<List<Subject>>(reader.ReadToEnd());
            }
        }

        public bool Delete(int id)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/subject/delete/"+id;

            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var response = await client.DeleteAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }
    }
}