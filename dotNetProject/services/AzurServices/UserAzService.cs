﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using dotNetProject.Model;
using dotNetProject.services.InterfaceService;
using Newtonsoft.Json;

namespace dotNetProject.services.AzurServices
{
    class UserAzService : IUserService
    {
        public bool Create(string nick, string password)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/users/Create/" + nick + "/" + password;

            var content = JsonConvert.SerializeObject(new { nick, password });

            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(URL, httpContent);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool Exists(string userNick)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/users/Exists/" + userNick;

            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool CheckPassword(string Password)
        {
            return CheckPassword((Application.Current.Properties["User"] as User).Name, Password);
        }

        public bool CheckPassword(string nick, string password)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/users/CheckPassword/"+nick+"/"+ password;

            //var content = JsonConvert.SerializeObject(new{nick,password});
            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                   // var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public User Select(int id)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/users/Get/" + id;


            using (var client = new HttpClient())
            {
                Task<User> task = Task.Run(async () =>
                {
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<User>(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public User Select(string setionHash)
        {
            throw new NotImplementedException();

            //            string URL = "http://noteapi20171206075553.azurewebsites.net/api/users/Select/" + setionHash;
            //
            //
            //            using (var client = new HttpClient())
            //            {
            //                Task<User> task = Task.Run(async () =>
            //                {
            //                    var response = await client.GetAsync(URL);
            //                    String responseText = await response.Content.ReadAsStringAsync();
            //                    return JsonConvert.DeserializeObject<User>(responseText);
            //                });
            //                task.Wait();
            //                return task.Result;
            //            }
        }

        public bool Delete(int id)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/users/delete/"+id ;

            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    

                    var response = await client.DeleteAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public User LogIn(string nick, string password)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/users/LogIn/" + nick + "/" + password;

//            var content = JsonConvert.SerializeObject(password);

            using (var client = new HttpClient())
            {
                Task<User> task = Task.Run(async () =>
                {
                    //var httpContent = new StringContent("="+password, Encoding.UTF8, "application/json");
                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<User>(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }

        public bool SetNewPassword(string newPassword)
        {
           return SetNewPassword(newPassword, (Application.Current.Properties["User"] as User).SetionUsrHash);
        }

        public bool SetNewPassword(string newPassword, string sessionHash)
        {
            string URL = "http://noteapi20171206075553.azurewebsites.net/api/users/SetNewPassword/" + newPassword+"/"+ sessionHash;
            //var content = JsonConvert.SerializeObject(new { newPassword, sessionHash });

            using (var client = new HttpClient())
            {
                Task<bool> task = Task.Run(async () =>
                {
                    //var httpContent = new StringContent(content, Encoding.UTF8, "application/json");

                    var response = await client.GetAsync(URL);
                    String responseText = await response.Content.ReadAsStringAsync();
                    return Convert.ToBoolean(responseText);
                });
                task.Wait();
                return task.Result;
            }
        }
    }
}
