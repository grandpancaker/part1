﻿using System.Collections.Generic;
using dotNetProject.Model;

namespace dotNetProject.services.InterfaceService
{
    public interface INotificationService
    {
        bool Create(Notification notification);
        bool Update(Notification notification);
        List<Notification> Select();
        List<Notification> Select(int NId);
        bool Delete(int id);
        bool DeleteByNote(int id);
    }
}