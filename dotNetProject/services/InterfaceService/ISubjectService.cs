﻿using System.Collections.Generic;
using dotNetProject.Model;

namespace dotNetProject.services.InterfaceService
{
    public interface ISubjectService
    {
        bool Create(Subject subject);
        bool Update(Subject subject);
        Subject Select(int id);
        List<Subject> SelectAll();
        bool Delete(int id);
    }
}