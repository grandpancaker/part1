using System.Collections.Generic;
using dotNetProject.Model;

namespace dotNetProject.services.InterfaceService
{
    public interface IAttachmentService
    {
        Attachment GetAttachment(int id);
        List<Attachment> GetAttachmentsForNote(int noteId);
        bool AddAttach(Attachment at, int noteId);
        bool Create(Attachment at);
        bool Delete(int id);
        bool DeleteByNote(int id);

    }
}