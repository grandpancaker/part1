﻿using System.Collections.Generic;
using dotNetProject.Model;

namespace dotNetProject.services.InterfaceService
{
    public interface INoteService
    {
        bool Create(Note note);
        bool Update(Note note);
        Note Select(int id);
        List<Note> SelectAll();
        List<Note> SelectAllForUser(int id);
        bool Delete(int id);
    }
}