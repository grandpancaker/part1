﻿using dotNetProject.Model;

namespace dotNetProject.services.InterfaceService
{
    public interface IUserService
    {
        bool Create(string Nick, string Password);
        bool Exists(string userNick);
        bool CheckPassword(string Password);
        bool CheckPassword(string nick, string password);
        User Select(int id);
        User Select(string setionHash);
        bool Delete(int id);
        User LogIn(string nick, string password);
        bool SetNewPassword(string newPassword);
        bool SetNewPassword(string newPassword, string sessionHash);
    }
}