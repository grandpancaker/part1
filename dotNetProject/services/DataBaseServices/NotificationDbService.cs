﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dotNetProject.Model;
using dotNetProject.services.InterfaceService;

namespace dotNetProject.services.DataBaseServices
{
    public class NotificationDbService : INotificationService
    {
        public bool Create(Notification notification)
        {
            using (var dbContext = new NotesContext())
            {
                dbContext.Notifications.Add(notification);
                return dbContext.SaveChanges() > 0;
            }
        }

        public bool Update(Notification notification)
        {
            using (var dbContext = new NotesContext())
            {

                return dbContext.SaveChanges() > 0;
            }
        }

        public List<Notification> Select()
        {
            using (var dbContext = new NotesContext())
            {
                return dbContext.Notifications.ToList();
            }
        }

        public List<Notification> Select(int NId)
        {
            using (var dbContext = new NotesContext())
            {
                return dbContext.Notifications.Where(n=>n.Note.NoteId == NId).ToList();
            }
        }

        public bool Delete(int id)
        {
            using (var dbContext = new NotesContext())
            {
                var tmp = dbContext.Notifications.Find(id);
                dbContext.Notifications.Remove(tmp);
                return dbContext.SaveChanges() > 0;
            }
        }

        public bool DeleteByNote(int id)
        {
            using (var dbContext = new NotesContext())
            {
                foreach (var tmp in dbContext.Notifications.Where(a => a.Note.NoteId == id))
                {
                    dbContext.Notifications.Remove(tmp);
                }

                return dbContext.SaveChanges() != 0;

            }
        }
    }
}
