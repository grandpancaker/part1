using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using dotNetProject.Model;
using dotNetProject.services.InterfaceService;

namespace dotNetProject.services.DataBaseServices
{
    public class NoteDbService : INoteService
    {
        private NotesContext dbContext = new NotesContext();
        public bool Create(Note note)
        {
            using (var dbContext = new NotesContext())
            {
                try
                {
                    dbContext.Notes.Add(note);
                    dbContext.SaveChanges();
                     
                    return true;
                }
                catch (Exception e)
                {
                       
                    return false;
                }
            }
        }

        public bool Update(Note note)
        {
            using (var dbContext = new NotesContext())
            {
                dbContext.Entry(note).State = EntityState.Modified;
                return dbContext.SaveChanges() > 0;
            }
        }

        public Note Select(int id)
        {
            
                return dbContext.Notes.Find(id);
            
        }

        public List<Note> SelectAll()
        {
           
                return dbContext.Notes.ToList();
            
        }

        public List<Note> SelectAllForUser(int id)
        {
            
                return dbContext.Notes.Where(n=> n.Users.Any(u=>u.UserId == id)).ToList();
            
        }

        public bool Delete(int id)
        {
            
                var attachmentService = new AttachmentDbService();
                var notificationService = new NotificationDbService();
                var note = dbContext.Notes.Find(id);

                    attachmentService.DeleteByNote(id);

                    notificationService.DeleteByNote(id);
                note.Notifications = null;
                note.Attachments = null;
                dbContext.Notes.Remove(dbContext.Notes.Find(id));
                return dbContext.SaveChanges() > 0;
            
        }
    }
}