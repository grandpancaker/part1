﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using dotNetProject.Model;
using dotNetProject.services.InterfaceService;

namespace dotNetProject.services.DataBaseServices
{
    public class UserDbService : IUserService
    {
        public bool Create(string Nick, string Password)
        {
            using (var dbContext = new NotesContext())
            {
                User u1 = new User(Nick, Password);
                dbContext.Users.Add(u1);
                return dbContext.SaveChanges() > 0;
            }
        }

        public bool Exists(string userNick)
        {
            using (var dbContext = new NotesContext())
            {
                return dbContext.Users.Any(u => u.Name == userNick);
            }
        }

        public bool CheckPassword(string Password)
        {
            var me = Application.Current.Properties["User"] as User;
            return me.ComperePassword(Password);

        }

        public bool CheckPassword(string nick, string password)
        {
            using (var dbContext = new NotesContext())
            {
                return dbContext.Users.First(u => u.Name.Equals(nick)).ComperePassword(password);
            }
        }

        public User Select(int id)
        {
            using (var dbContext = new NotesContext())
            {
                return dbContext.Users.Find(id);
            }
        }

        public User Select(string setionHash)
        {
            using (var dbContext = new NotesContext())
            {
                return dbContext.Users.First(u => u.SetionUsrHash.Equals(setionHash));
            }
        }

        public bool Delete(int id)
        {
            using (var dbContext = new NotesContext())
            {
                
            }
            throw new NotImplementedException();
        }

        public User LogIn(string nick, string password)
        {
            using (var dbContext = new NotesContext())
            {
              
                
                var user = dbContext.Users.First(u => u.Name.Equals(nick));
                
                if (user.ComperePassword(password))
                {
                    Guid g = Guid.NewGuid();
                    string GuidString = Convert.ToBase64String(g.ToByteArray());
                    GuidString = GuidString.Replace("=", "");
                    GuidString = GuidString.Replace("+", "");
                    GuidString = GuidString.Replace("/", "");
                        GuidString.Split(new char[]{ ';', '/', '?', ':', '@', '=', '&' });

                    dbContext.Entry(user).CurrentValues.SetValues(user.SetionUsrHash = GuidString);
                    dbContext.SaveChanges();
                    return user;
                }
                else
                {
                    return null;
                }
            }
        }

        public bool SetNewPassword(string newPassword)
        {
            using (var dbContext = new NotesContext())
            {
                var me = Application.Current.Properties["User"] as User;
                me.SetNewPassword(newPassword);
                dbContext.Entry(dbContext.Users.Find(me.UserId)).CurrentValues.SetValues(me);
                return dbContext.SaveChanges() > 0;
            }
            
        }
        public bool SetNewPassword(string newPassword, string sessionHash)
        {
            using (var dbContext = new NotesContext())
            {
                var me = Select(sessionHash);
                me.SetNewPassword(newPassword);
                dbContext.Entry(dbContext.Users.Find(me.UserId)).CurrentValues.SetValues(me);
                return dbContext.SaveChanges() > 0;
            }

        }
    }
}
