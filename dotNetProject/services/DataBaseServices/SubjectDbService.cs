﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using dotNetProject.Model;
using dotNetProject.services.InterfaceService;

namespace dotNetProject.services.DataBaseServices
{
    public class SubjectDbService : ISubjectService
    {
        public bool Create(Subject subject)
        {
            using (var dbContext = new NotesContext())
            {
                dbContext.Subjects.Add(subject);
                return dbContext.SaveChanges() > 0;
            }
        }

        public bool Update(Subject subject)
        {
            using (var dbContext = new NotesContext())
            {
                var tmp = dbContext.Subjects.Find(subject);
                tmp.Name = subject.Name;
                tmp.Notes = subject.Notes;
                dbContext.Entry(tmp).State = EntityState.Modified;

                return dbContext.SaveChanges() > 0;
            }

        }

        public Subject Select(int id)
        {
            using (var dbContext = new NotesContext())
            {
                return dbContext.Subjects.Find(id);
            }
        }

        public List<Subject> SelectAll()
        {
            using (var dbContext = new NotesContext())
            {
                return dbContext.Subjects.ToList();
            }

        }

        public bool Delete(int id)
        {
            using (var dbContext = new NotesContext())
            {
                var tmp = dbContext.Subjects.Find(id);

                dbContext.Subjects.Remove(tmp);

                return dbContext.SaveChanges() > 0;
            }
            throw new System.NotImplementedException();
        }
    }
}