﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using dotNetProject.Model;
using System.Data.Entity;
using System.Diagnostics;

namespace dotNetProject
{
    /// <summary>
    /// Interaction logic for NoteView.xaml
    /// </summary>
    public partial class NoteView : Window
    {
        public int NID = -1;
        public static List<Notification> listn = new List<Notification>();
        List<Attachment> lista = new List<Attachment>();

        public NoteView()
        {
            var user = Application.Current.Properties["User"] as User;

            InitializeComponent();
            BasicInit();
        }

        public NoteView(int ID)
        {
            InitializeComponent();
            BasicInit();
            NID = ID;
            ExistingInit();
        }

        private void BasicInit()
        {
            Author.Text = (Application.Current.Properties["User"] as User).Name;
            Categories.ItemsSource = Enum.GetValues(typeof(Importance)).Cast<Importance>();
            Categories.SelectedIndex = 0;
            ClassTypes.ItemsSource = Enum.GetValues(typeof(ClassType)).Cast<ClassType>();
            ClassTypes.SelectedIndex = 0;
        }

        private void ExistingInit()
        {
            DeleteButton.IsEnabled = true;
            using (var db = new NotesContext())
            {

                Note note = db.Notes.Where(n => n.NoteId == NID).FirstOrDefault<Note>();

                NoteName.Text = note.Name;
                UpdateDate.Text = note.UpdateDate.ToString();
                CreateDate.Text = note.CreationDate.ToString();
                Categories.SelectedValue = note.Category.Importance;
                ClassTypes.SelectedValue = note.Category.ClassType;
                noteText.AppendText(note.Text);

                db.Notifications.Load();
                ListNotif.ItemsSource = note.Notifications;

                db.Attachments.Load();
                ListAtt.ItemsSource = note.Attachments;
            }

        }

        private void OpenImage(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ListBoxItem MyItem = ListAtt.ItemContainerGenerator.ContainerFromItem(ListAtt.SelectedItem) as ListBoxItem;
                Attachment at = MyItem.Content as Attachment;
                var view = new AttachmentView(NID, at.AttachmentId);
                view.Show();
            }
            catch (FormatException) { }
        }

        private void SaveClick(object sender, MouseButtonEventArgs e)
        {
            using (var db = new NotesContext())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (NID == -1)
                        {

                            List<User> Users = new List<User>();
                            Users.Add(db.Users.Find((Application.Current.Properties["User"] as User).UserId));
                            Importance imp;
                            ClassType type;
                            try
                            {
                                imp = (Importance)Categories.SelectedIndex;
                                type = (ClassType)ClassTypes.SelectedIndex;
                            }
                            catch (NullReferenceException)
                            {
                                MessageBox.Show("Choose category and class type");
                                return;
                            }
                            Category cat = new Category();
                            cat.Importance = imp;
                            cat.ClassType = type;

                            Note note = new Note { Text = GetTextRTB(noteText), Name = NoteName.Text, CreationDate = DateTime.Now.Date, UpdateDate = DateTime.Now.Date, Users = Users, Category = cat, Attachments = lista, Notifications = listn };

                            foreach (var att in lista)
                            {
                                db.Attachments.Add(att);
                            }
                            foreach (var notif in listn)
                            {
                                db.Notifications.Add(notif);
                            }

                            db.Notes.Add(note);
                            db.SaveChanges();
                        }
                        else
                        {
                            Importance imp;
                            ClassType type;
                            try
                            {
                                imp = (Importance)Categories.SelectedIndex;
                                type = (ClassType)ClassTypes.SelectedIndex;
                            }
                            catch (NullReferenceException)
                            {
                                MessageBox.Show("Choose category and class type");
                                return;
                            }
                            Category cat = new Category();
                            cat.Importance = imp;
                            cat.ClassType = type;

                            Note note = db.Notes.Find(NID);
                            note.Text = GetTextRTB(noteText);
                            note.Name = NoteName.Text;
                            note.UpdateDate = DateTime.Now.Date;
                            note.Category = cat;
                            foreach (var att in lista)
                            {
                                db.Attachments.Add(att);
                            }
                            foreach (var notif in listn)
                            {
                                db.Notifications.Add(notif);
                            }

                            note.Notifications = listn;
                            note.Attachments = lista;

                            db.Entry(note).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        dbContextTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().RefreshNoteList();
            this.Close();
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {
            using (var db = new NotesContext())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Note note = db.Notes.Find(NID);
                        var att = db.Attachments.Where(a => a.Note.NoteId == note.NoteId);
                        var notif = db.Notifications.Where(n => n.Note.NoteId == note.NoteId);

                        foreach (var a in att)
                        {
                            db.Attachments.Remove(a);
                        }
                        foreach (var a in notif)
                        {
                            db.Notifications.Remove(a);
                        }
                        note.Notifications = null;
                        note.Attachments = null;

                        db.Notes.Remove(note);
                        db.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                    }
                }

            }
            Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().RefreshNoteList();
            this.Close();
        }

        private void AddAttach(object sender, MouseButtonEventArgs e)
        {
            var newFileDialog = new Microsoft.Win32.OpenFileDialog();
            newFileDialog.Filter = "Image files (*.png;*.jpeg)|*.png;*.jpeg|All files (*.*)|*.*";
            Nullable<bool> result = newFileDialog.ShowDialog();
            if (result == true)
            {
                string fname = newFileDialog.FileName;

                var at = new Attachment();
                System.Drawing.Image image = System.Drawing.Image.FromFile(newFileDialog.FileName);
                at.SavePicture(image);
                lista.Add(at);
            }


        }

        private void AddNotif(object sender, MouseButtonEventArgs e)
        {
            var sf = new SetNotif(NID);
            sf.Show();
        }

        public string GetTextRTB(RichTextBox rtb)
        {
            var textRange = new TextRange(rtb.Document.ContentStart, rtb.Document.ContentEnd);
            return textRange.Text;
        }

        private void CommentClick(object sender, MouseButtonEventArgs e)
        {
            var com = new Comment();
            com.Left = this.Top - 20;
            com.Show();
        }
    }
}
