﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;


namespace dotNetProject.Model
{
    public class Message
    {
        [Key] public Int64 MessageId { get; set; }
        public string Text { get; set; }
        public Int64 Author { get; set; }
        public DateTime Time { get; set; }
        public Message() { }
        public Message(string text, Int64 author)
        {
            Text = text;
            Author = author;
            Time = DateTime.Now;
        }
    }
}
