﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;


namespace dotNetProject.Model
{
    public class Dialog
    {
        [Key] public Int64 DialogId { get; set; }
        public int NoteId { get; set; }
        public Int64 Sender { get; set; }
        public Int64 Receiver { get; set; } //author of the note being commented
        public ObservableCollection<Message> Messages { get; set; }
        public Dialog() { }
        public Dialog(Int64 sender, Int64 receiver, int note)
        {
            NoteId = note;
            Sender = sender;
            Receiver = receiver;
        }
    }
}
