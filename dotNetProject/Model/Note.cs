﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;


namespace dotNetProject.Model
{
    public class Note
    {
        [Key] public int NoteId { get; set; }
        public string Name { get; set; }
        public Category Category { get; set; }
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }

    }
}