﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using dotNetProject.Model;
using System.Data.Entity;
using Microsoft.AspNet.SignalR.Client;
using dotNetProject.services.InterfaceService;
using dotNetProject.services.AzurServices;

namespace dotNetProject
{
    /// <summary>
    /// Interaction logic for NoteView.xaml
    /// </summary>
    public partial class NoteView : Window
    {
        public int NID = -1;
        private INoteService noteService = new NoteAzService();
        private IAttachmentService attachmentService = new AttachmentAzService();
        private INotificationService notificationService = new NotificationAzService();
        IHubProxy hubProxy;
        public static List<Notification> listn = new List<Notification>();
        public List<Attachment> lista = new List<Attachment>();
        public NoteView(IHubProxy hub)
        {
            var user = Application.Current.Properties["User"] as User;
            lista.Clear();
            listn.Clear();
            InitializeComponent();
            hubProxy = hub;
            BasicInit();
        }

        public NoteView(IHubProxy hub, int ID)
        {
            InitializeComponent();
            hubProxy = hub;
            BasicInit();
            NID = ID;
            ExistingInit();
        }

        private void BasicInit()
        {
            Author.Text = (Application.Current.Properties["User"] as User).Name;
            Categories.ItemsSource = Enum.GetValues(typeof(Importance)).Cast<Importance>();
            Categories.SelectedIndex = 0;
            ClassTypes.ItemsSource = Enum.GetValues(typeof(ClassType)).Cast<ClassType>();
            ClassTypes.SelectedIndex = 0;
        }

        private void ExistingInit()
        {
            DeleteButton.IsEnabled = true;


            Note note = noteService.Select(NID);

            NoteName.Text            = note.Name;
            UpdateDate.Text          = note.UpdateDate.ToString();
            CreateDate.Text          = note.CreationDate.ToString();
            Categories.SelectedValue = note.Category.Importance;
            ClassTypes.SelectedValue = note.Category.ClassType;
            noteText.AppendText(note.Text);

           
            ListNotif.ItemsSource = note.Notifications;

            
            ListAtt.ItemsSource = note.Attachments;
            
            
        }

        private void OpenImage(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ListBoxItem MyItem = ListAtt.ItemContainerGenerator.ContainerFromItem(ListAtt.SelectedItem) as ListBoxItem;
                Attachment at = MyItem.Content as Attachment;
                var view = new AttachmentView(NID, at.AttachmentId);
                view.Show();
            }
            catch (FormatException) { }
        }

        private void SaveClick(object sender, MouseButtonEventArgs e)
        {

            try
            {
                if (NID == -1)
                {

                    List<User> Users = new List<User>();
                    Users.Add(Application.Current.Properties["User"] as User);
                    Importance imp;
                    ClassType type;
                    try
                    {
                        imp = (Importance)Categories.SelectedIndex;
                        type = (ClassType)ClassTypes.SelectedIndex;
                    }
                    catch (NullReferenceException)
                    {
                        MessageBox.Show("Choose category and class type");
                        return;
                    }
                    Category cat = new Category();
                    cat.Importance = imp;
                    cat.ClassType = type;

                    Note note = new Note { Text = GetTextRTB(noteText), Name = NoteName.Text, CreationDate = DateTime.Now.Date, UpdateDate = DateTime.Now.Date, Users = Users, Category = cat, Attachments = lista, Notifications = listn };
//
//                    foreach (var att in lista)
//                    {
//                        note.Attachments.Add(att);
//                    }
//                    foreach (var notif in listn)
//                    {
//                        note.Notifications.Add(notif);
//                    }

                    noteService.Create(note);
                }
                else
                {
                    Importance imp;
                    ClassType type;
                    try
                    {
                        imp = (Importance)Categories.SelectedIndex;
                        type = (ClassType)ClassTypes.SelectedIndex;
                    }
                    catch (NullReferenceException)
                    {
                        MessageBox.Show("Choose category and class type");
                        return;
                    }
                    Category cat = new Category();
                    cat.Importance = imp;
                    cat.ClassType = type;

                    Note note = noteService.Select(NID);
                    note.Text = GetTextRTB(noteText);
                    note.Name = NoteName.Text;
                    note.UpdateDate = DateTime.Now.Date;
                    note.Category = cat;

                    foreach (var att in lista)
                    {
                        note.Attachments.Add(att);
                    }
                    foreach (var notif in listn)
                    {
                        note.Notifications.Add(notif);
                    }

//                    note.Notifications = listn;
//                    note.Attachments = lista;

                    noteService.Update(note);
                }
                        
            }
            catch (Exception ea)
            {
                MessageBox.Show(ea.Message.ToString());
            }
            Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().RefreshNoteList();
            this.Close();
        }

        private void DeleteClick(object sender, MouseButtonEventArgs e)
        {

                    try
                    {
//                        Note note = noteService.Select(NID);
//                        var att = attachmentService.GetAttachmentsForNote(NID);
//                        var notif = notificationService.Select(NID);

                      

                        noteService.Delete(NID);
                    }
                    catch (Exception)
                    {
                    }

            Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().RefreshNoteList();
            this.Close();
        }

        private void AddAttach(object sender, MouseButtonEventArgs e)
        {
            var newFileDialog = new Microsoft.Win32.OpenFileDialog();
            newFileDialog.Filter = "Image files (*.png;*.jpeg)|*.png;*.jpeg|All files (*.*)|*.*";
            Nullable<bool> result = newFileDialog.ShowDialog();
            if (result == true)
            {
                string fname = newFileDialog.FileName;
                var at = new Attachment();

                System.Drawing.Image image = System.Drawing.Image.FromFile(newFileDialog.FileName);
                at.SavePicture(image);
                lista.Add(at);
                //                using (var db = new NotesContext())
                //                {
                //                    Note note = db.Notes.Find(NID);
                //                    if (note.Attachments == null) note.Attachments = new List<Attachment>();
                //                    var at = new Attachment();
                //                    System.Drawing.Image image = System.Drawing.Image.FromFile(newFileDialog.FileName);
                //                    at.SavePicture(image);
                //
                //                    note.Attachments.Add(at);
                //                    db.Entry(note).State = EntityState.Modified;
                //                    db.SaveChanges();
                //                }
            }

        }

        private void AddNotif(object sender, MouseButtonEventArgs e)
        {
            var sf = new SetNotif(NID);
            sf.Show();
        }

        public string GetTextRTB(RichTextBox rtb)
        {
            var textRange = new TextRange(rtb.Document.ContentStart, rtb.Document.ContentEnd);
            return textRange.Text;
        }

        private void CommentClick(object sender, MouseButtonEventArgs e)
        {
            Note note = noteService.Select(NID);
            var com = new Comment(hubProxy, NID, (int)note.Users.ElementAt(0).UserId, (int)(Application.Current.Properties["User"] as User).UserId);
            com.Left = this.Top - 20;
            com.Show();
        }
    }
}
