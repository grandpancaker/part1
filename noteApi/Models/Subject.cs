﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace dotNetProject.Model
{
    public class Subject
    {
        [Key] public int SubjectId { get; set; }
        public String Name { get; set; }
        public ObservableCollection<Note> Notes { get; set; }
    }
}