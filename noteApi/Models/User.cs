﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;


namespace dotNetProject.Model
{
    public class User
    {
        [Key] public Int64 UserId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public ObservableCollection<Note> Notes { get; set; }
        public User() { }
        public User(string _Name, string _Password)
        {
            Name = _Name;
            Password = Hash(_Password);
            
        }
        static string Hash(string input)
        {
            var hash = (new SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(input));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        }

        public bool ComperePassword(string _Password)
        {
            return Password.Equals(Hash(_Password));
        }

        public void SetNewPassword(string _Password)
        {
            Password = Hash(_Password);
        }
    }
}