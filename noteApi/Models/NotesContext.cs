﻿
using System;
using System.Collections.Generic;
using dotNetProject.Model;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Drawing;
using System.Globalization;


using System.Linq;

namespace dotNetProject.Model
{
    public class NotesContext : DbContext
    {
        public NotesContext() : base("name=AzureNotes")
        {
            Database.SetInitializer<NotesContext>(new DropCreateDatabaseIfModelChanges<NotesContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Configure domain classes using Fluent API here

            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Attachment> Attachments { get; set; }
        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<User> Users { get; set; }

       
    }

   
}