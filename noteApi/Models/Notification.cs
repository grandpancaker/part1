﻿using System;
using System.ComponentModel.DataAnnotations;


namespace dotNetProject.Model
{
    public class Notification
    {
        [Key]
        public int NotificationId { get; set; }
        public DateTime Date { get; set; }
        public virtual Note Note { get; set; }

    }
}