﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using dotNetProject.Model;
using dotNetProject.services.DataBaseServices;
using dotNetProject.services.InterfaceService;

namespace noteApi.Controllers.api
{
    [RoutePrefix("api/Notification")]
    public class NotificationsController : ApiController
    {
        INotificationService notificationService = new NotificationDbService();

        [HttpPost]
        [Route("Create")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Create(Notification notification)
        {
            return Ok(notificationService.Create(notification));
        }


        [HttpPost]
        [Route("Update")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Update(Notification notification)
        {
            return Ok(notificationService.Update(notification));
        }

        [HttpGet]
        [Route("Select/{NId}")]
        [ResponseType(typeof(Notification))]
        public IHttpActionResult Select(int NId)
        {
            return Ok(notificationService.Select(NId));
        }

        [HttpGet]
        [Route("SelectAll")]
        [ResponseType(typeof(List<Notification>))]
        public IHttpActionResult Sellect()
        {
            return Ok(notificationService.Select());
        }


        [HttpDelete]
        [Route("Delete/{id}")]
        [ResponseType(typeof(Subject))]
        public IHttpActionResult Delete(int id)
        {
            return Ok(notificationService.Delete(id));
        }

    }
}