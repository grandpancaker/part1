﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using dotNetProject.Model;
using dotNetProject.services.DataBaseServices;
using dotNetProject.services.InterfaceService;

namespace noteApi.Controllers.api
{
    [RoutePrefix("api/subject")]
    public class SubjectsController : ApiController
    {
        ISubjectService subjectService = new SubjectDbService();

        [HttpPost]
        [Route("Create")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Create([FromBody] Subject subject)
        {
            return Ok(subjectService.Create(subject));
        }

        [HttpPost]
        [Route("Update")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Update([FromBody] Subject subject)
        {
            return Ok(subjectService.Update(subject));
        }

        [HttpGet]
        [Route("Select/{id}")]
        [ResponseType(typeof(Subject))]
        public IHttpActionResult Select(int id)
        {
            return Ok(subjectService.Select(id));
        }

        [HttpGet]
        [Route("SelectAll")]
        [ResponseType(typeof(List<Subject>))]
        public IHttpActionResult SellectAll()
        {
            return Ok(subjectService.SelectAll());
        }


        [HttpDelete]
        [Route("Delete/{id}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Delete(int id)
        {
            return Ok(subjectService.Delete(id));
        }
    }
}