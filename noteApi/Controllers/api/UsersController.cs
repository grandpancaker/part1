﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using dotNetProject.Model;
using dotNetProject.services.DataBaseServices;
using dotNetProject.services.InterfaceService;


namespace noteApi.Controllers.api
{
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        private IUserService userService = new UserDbService();
        
        [HttpGet]
        [ResponseType(typeof(bool))]
        [Route("Create/{nick}/{password}")]
        public IHttpActionResult Post(string nick, string password)
        {
           
                return Ok(userService.Create(nick, password));
           
        }
        [HttpGet]
        [ResponseType(typeof(bool))]
        [Route("Exists/{nick}")]
        public IHttpActionResult Exists(string nick)
        {
            return Ok(userService.Exists(nick));
           
        }
        [HttpGet]
        [ResponseType(typeof(bool))]
        [Route("CheckPassword/{nick}/{password}")]
        public IHttpActionResult CheckPassword(String nick,String password)
        {
            return Ok(userService.CheckPassword(nick,password));

        }


        // GET: api/Users/5
        [HttpGet]
        [ResponseType(typeof(User))]
        [Route("get/{id}")]
        public IHttpActionResult Get(int id)
        {
            User user = userService.Select(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(bool))]
        [HttpGet]
        [Route("SetNewPassword/{newPassword}/{sessionUsrHash}")]
        public IHttpActionResult SetNewPassword(string newPassword, string sessionUsrHash)
        {
            var test = userService.SetNewPassword(newPassword, sessionUsrHash);
            return Ok(test);
        }
        [HttpGet]
        [ResponseType(typeof(User))]
        [Route("LogIn/{nick}/{password}")]
        public IHttpActionResult LogIn(string nick, string password)
        {
            var user = userService.LogIn(nick, password);
           return Ok(user);
        }

    }
}