﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using dotNetProject.Model;
using dotNetProject.services.AzurServices;
using dotNetProject.services.DataBaseServices;
using dotNetProject.services.InterfaceService;

namespace noteApi.Controllers.api
{
    [RoutePrefix("api/Note")]
    public class NotesController : ApiController
    {
        INoteService noteService = new NoteDbService();

        [HttpPost]
        [Route("Create")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Create(Note note)
        {
            return Ok(noteService.Create(note));
        }

        [HttpPost]
        [Route("Update")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Update(Note note)
        {
            return Ok(noteService.Update(note));
        }

        [HttpGet]
        [Route("Select/{id}")]
        [ResponseType(typeof(Note))]
        public IHttpActionResult Select(int id)
        {
            return Ok(noteService.Select(id));
        }

        [HttpGet]
        [Route("SelectAll")]
        [ResponseType(typeof(List<Note>))]
        public IHttpActionResult SellectAll()
        {
            return Ok(noteService.SelectAll());
        }

        [HttpGet]
        [Route("SelectAllForUser/{userId}")]
        [ResponseType(typeof(List<Note>))]
        public IHttpActionResult SellectAllForUser(int userId)
        {
            return Ok(noteService.SelectAllForUser(userId));
        }


        [HttpDelete]
        [Route("Delete/{id}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Delete(int id)
        {
            return Ok(noteService.Delete(id));
        }
    }
}