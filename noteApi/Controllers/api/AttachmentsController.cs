﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using dotNetProject.Model;
using dotNetProject.services.DataBaseServices;
using dotNetProject.services.InterfaceService;

namespace noteApi.Controllers.api
{
    [RoutePrefix("api/Attachment")]
    public class AttachmentsController : ApiController
    {
        IAttachmentService attachmentService = new AttachmentDbService();

        [HttpPost]
        [Route("Create")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Create(Attachment attachment)
        {
            return Ok(attachmentService.Create(attachment));
        }

        [HttpPost]
        [Route("AddAttach/{noteId}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Update(int noteId,Attachment attachment)
        {
            return Ok(attachmentService.AddAttach(attachment,noteId));
        }

        [HttpGet]
        [Route("GetAttachment/{id}")]
        [ResponseType(typeof(Attachment))]
        public IHttpActionResult GetAttachment(int id)
        {
            return Ok(attachmentService.GetAttachment(id));
        }

        [HttpGet]
        [Route("GetAttachmentsForNote/{noteId}")]
        [ResponseType(typeof(List<Attachment>))]
        public IHttpActionResult GetAttachmentsForNote(int noteId)
        {
            return Ok(attachmentService.GetAttachmentsForNote(noteId));
        }


        [HttpDelete]
        [Route("Delete/{id}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Delete(int id)
        {
            return Ok(attachmentService.Delete(id));
        }
    }
}